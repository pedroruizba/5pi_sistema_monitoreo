
#include "DHT.h"// libreria sensor dht11
#define DHTPIN 2     // Pin donde está conectado el sensor dht11
#define DHTTYPE DHT11   // sensor dht11
DHT dht(DHTPIN, DHTTYPE);//sensor dht11

//incluyes librerias
#include <VirtualWire.h>//libreria para la RF

#include <MPU6050.h>

#include <Wire.h>

#define minval -150

#define maxval 0

MPU6050 mpu;
const int buzzer = 7;

void setup()

{   //Establece la velocidad de datos en bits por segundo, para la transmisión de datos en serie.
    Serial.begin(115200);
    //selecciona pin que se utilizara
    //pinMode(buzzer, OUTPUT); // Set buzzer - pin 7 as an output
    dht.begin();//dht11 iniciando
    pinMode(7,OUTPUT);
    pinMode(8,OUTPUT);
    pinMode(13,OUTPUT);
    vw_setup(2000);//iniciamos comunicacion por RF
    vw_set_tx_pin(buzzer);
// Initialize MPU6050

  while(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G))

  { Serial.println("Could not find a valid MPU6050 sensor, check wiring!");

    delay(500);}

  mpu.setThreshold(3); 

  // Check settings

  checkSettings();

}


void checkSettings()

{
  switch(mpu.getClockSource())

  {case MPU6050_CLOCK_KEEP_RESET:     Serial.println("Stops the clock and keeps the timing generator in reset"); break;

    case MPU6050_CLOCK_EXTERNAL_19MHZ: Serial.println("PLL with external 19.2MHz reference"); break;

    case MPU6050_CLOCK_EXTERNAL_32KHZ: Serial.println("PLL with external 32.768kHz reference"); break;

    case MPU6050_CLOCK_PLL_ZGYRO:      Serial.println("PLL with Z axis gyroscope reference"); break;

    case MPU6050_CLOCK_PLL_YGYRO:      Serial.println("PLL with Y axis gyroscope reference"); break;

    case MPU6050_CLOCK_PLL_XGYRO:      Serial.println("PLL with X axis gyroscope reference"); break;

    case MPU6050_CLOCK_INTERNAL_8MHZ:  Serial.println("Internal 8MHz oscillator"); break;

  }

  Serial.print(" * Gyroscope:         ");

  switch(mpu.getScale())

  {case MPU6050_SCALE_2000DPS:        Serial.println("2000 dps"); break;

    case MPU6050_SCALE_1000DPS:        Serial.println("1000 dps"); break;

    case MPU6050_SCALE_500DPS:         Serial.println("500 dps"); break;

    case MPU6050_SCALE_250DPS:         Serial.println("250 dps"); break;}

}

void loop()

{ digitalWrite(13,LOW);  
  
  Vector rawGyro = mpu.readRawGyro();

  Vector normGyro = mpu.readNormalizeGyro();
String str;  
    char buf[VW_MAX_MESSAGE_LEN];
// if(normGyro.XAxis > maxval || normGyro.XAxis < minval && normGyro.YAxis > maxval || normGyro.YAxis  < minval && normGyro.ZAxis > maxval || normGyro.ZAxis  < minval)
if(normGyro.XAxis > maxval || normGyro.XAxis < minval && normGyro.YAxis > maxval || normGyro.YAxis  < minval && normGyro.ZAxis > maxval || normGyro.ZAxis  < minval)
{
  //para humedad y temperatura
    float h = dht.readHumidity(); //Leemos la Humedad
    float t = dht.readTemperature(); //Leemos la temperatura en grados Celsius
    //float f = dht.readTemperature(true); //Leemos la temperatura en grados Fahrenheit
    /*Serial.print("Humedad ");
    Serial.print(h);
    Serial.print(" %t");
    Serial.print("Temperatura: ");
    Serial.print(t);
    Serial.print(" *C ");
    Serial.print(f);
    Serial.println(" *F");*/
    
    //para humedad y temperatura
    //str = "f" + String(normGyro.XAxis); // Convertir a string
    float magnitud=sqrt(pow(normGyro.XAxis,2)+pow(normGyro.YAxis,2));
    str ="a"+String(magnitud)+"#"+String(h)+"#"+String(t)+"#"; // Convertir a string
    str.toCharArray(buf,sizeof(buf)); // Convertir a char array
    vw_send((uint8_t *)buf, strlen(buf)); // Enviar array
    vw_wait_tx(); // Esperar envio   
    delay(200);
    Serial.println(str);
//Serial.println(normGyro.XAxis);
//Serial.println(normGyro.YAxis);
//Serial.println(normGyro.ZAxis);

  //delay(100); // espera 300 milisegundos
  tone(buzzer, 1100);
  delay(1000);
  noTone(buzzer); 
}

 /*else{
  digitalWrite(7,LOW);
//digitalWrite(8,LOW);}
str = "f" + String(normGyro.XAxis); // Convertir a string
    str.toCharArray(buf,sizeof(buf)); // Convertir a char array
    vw_send((uint8_t *)buf, strlen(buf)); // Enviar array
    vw_wait_tx(); // Esperar envio   
    delay(200);
//Serial.println(normGyro.XAxis);
//Serial.println(normGyro.YAxis);
//Serial.println(normGyro.ZAxis);

 delay(30);
  }//espera 30 */
 }
